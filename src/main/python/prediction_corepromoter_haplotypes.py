#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 12:44:55 2018

@author: mm2842
"""
import sys
import glob

import pandas as pd
import numpy as np

import datetime
import warnings
import javabridge

import scipy.signal
from math import log
from itertools import product
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.externals import joblib

from multiprocessing import Pool
from multiprocessing import cpu_count


from progress.bar import Bar

warnings.filterwarnings("ignore")

#setters
def set_kmerlength(value):
    global KSIZE
    KSIZE = value
    
def set_winsize(value):
    global WINDOWSIZE
    WINDOWSIZE = value

def set_overlap(value):
    global OVERLAP
    OVERLAP = value
    
def set_includeSequence(value):
    global SEQUENCE
    SEQUENCE = value

def set_includeVariants(value):
    global VARIANTS
    VARIANTS = value    

#getters    
def get_kmerlength():
    return KSIZE

def get_winsize():
    return WINDOWSIZE 

def get_overlap():
    return OVERLAP

def get_includeSequence():
    return SEQUENCE

def get_includeVariants():
    return VARIANTS


#helpers
def datetimenow():
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

def make_newtoken(kmer):
    kmer = kmer.lower()
    newtoken = "n".join(sorted([kmer,kmer.translate(str.maketrans('tagc', 'atcg'))[::-1]]))
    return newtoken

def create_kmer_set(kmerlength=8):
    kmer_set = set()
    nucleotides = ["a", "c", "g", "t"]
    kmerall = product(nucleotides, repeat=kmerlength)
    for i in kmerall:
        kmer = ''.join(i)
        kmer_set.add(kmer)
    uniq_kmers = sorted(list(kmer_set))
    return uniq_kmers 

def create_newtoken_set(kmerlength=8):
    newtokenSet = set()
    uniq_kmers = create_kmer_set(kmerlength=kmerlength)
    for kmer in uniq_kmers:
        newtoken = make_newtoken(kmer)
        newtokenSet.add(newtoken)
    uniq_newtokens = sorted(list(newtokenSet))
    return uniq_newtokens


def compute_kmer_entropy(kmer):
    prob = [float(kmer.count(c)) / len(kmer) for c in dict.fromkeys(list(kmer))]
    entropy = - sum([p * log(p) / log(2.0) for p in prob])
    return round(entropy, 2)

def make_stopwords(kmerlength=8):
    kmersize_filter = {5: 1.3, 6: 1.3, 7: 1.3, 8: 1.3, 9: 1.3, 10: 1.3} #the entropy with the k-mers values should be modeled
    limit_entropy = kmersize_filter.get(kmerlength)
    kmerSet = set()
    nucleotides = ["a", "c", "g", "t"]
    kmerall = product(nucleotides, repeat=kmerlength)
    for n in kmerall:
        kmer = ''.join(n)

        if compute_kmer_entropy(kmer) < limit_entropy:
            kmerSet.add(make_newtoken(kmer))
        else:
            continue
    stopwords = sorted(list(kmerSet))
    return stopwords

def building_vocabulary(full_mode=False):
    print("{}: Building a vocabulary from tokens".format(datetimenow()))
    kmerlength=get_kmerlength() 
    all_tokens = create_newtoken_set(kmerlength=kmerlength)
    tmpvectorizer = TfidfVectorizer(min_df=1, max_df=1.0, sublinear_tf=True, use_idf=True)
    _ = tmpvectorizer.fit_transform(all_tokens)
    vcblry = tmpvectorizer.get_feature_names()

    if full_mode:
        print("{}: Keeping all low-complexity k-mers".format(datetimenow()))
        kmer_names = vcblry
    else:
        stpwrds = make_stopwords(kmerlength=kmerlength)
        print("{}: Removing {} low-complexity k-mers".format(datetimenow(), len(stpwrds)))
        kmer_names = [x for x in vcblry if x not in stpwrds]

    # Check that tokens are as many as expected math.pow(4, kmerlength)/2
    if len(kmer_names) > len(all_tokens):
        print("{}: ERROR: Expected {} tokens. Obtained {} tokens".format(datetimenow(), len(all_tokens), len(kmer_names)))
        quit()
    else:
        print("{}: Expected {} tokens. Obtained {} tokens".format(datetimenow(), len(all_tokens), len(kmer_names)))
    return kmer_names

def write_ngrams(sequence, tokenize=True):
    kmerlength=get_kmerlength() 
    seq = str(sequence).lower()
    finalstart = (len(seq) - kmerlength) + 1
    allkmers = [seq[start:(start + kmerlength)] for start in range(0, finalstart)]
    if tokenize:
        tokens = [make_newtoken(kmer) for kmer in allkmers if len(kmer) == kmerlength and "n" not in kmer]
        newtoken_string = " ".join(tokens)
        return newtoken_string
    else:
        return allkmers


def index_generator(star, end, winSize=1, overlap=1): 
    seq = list(range(star,end))
    if winSize < 1 or overlap < 0:
        raise ValueError('size must be >= 1 and overlap >= 0')

    for i in range(0, len(seq) - overlap, winSize - overlap):
        if len(seq[i:i + winSize]) == winSize:
            start = seq[i:i + winSize][0]
            end = seq[i:i + winSize][-1]+1
            yield start, end
            

def slice_haplotype_sequence(df):
    evaluations = list()
    for idx, row in df.iterrows():
        winsize = get_winsize()
        overlap = get_overlap()
        idx_window = 0
        for coords in index_generator(0, len(row.sequence), winsize, overlap=overlap):
            ref_string = row.sequence[coords[0]:coords[1]]
            if ref_string.count('N') <= int(winsize/4): 
                tokens = write_ngrams(ref_string)
                evaluations.append([row.idx_row,
                            idx_window,
                            row.chr,
                            coords[0],
                            coords[1],
                            ref_string,
                            tokens,
                            row.hapId,
                            row.taxaList])
            idx_window += 1
    return evaluations


def filter_probs_to_df_group(tmp_df_group):
    indexes, _ = scipy.signal.find_peaks(tmp_df_group['probability'].values.flatten(), height=0.84, distance=1)
    elements = list()
    #evaluate the first and last window which are not evaluated with the peakfinder function
    if tmp_df_group['probability'].values.tolist()[0] >= 0.85 :
        elements.append(0)
    if tmp_df_group['probability'].values.tolist()[-1] >= 0.85 :
        elements.append(len(tmp_df_group['probability'].values.tolist()) - 1)
    np.append(indexes, elements)
    if indexes.tolist():
        return tmp_df_group.reset_index(drop=True).loc[indexes] #be sure that unique index are returned
    else:
        return pd.DataFrame(columns=list(tmp_df_group.reset_index(drop=True))) #empty dataframe
    

#functions to decrease the looping using embarrassing parallelization start with mp
def mp_slice_haplotype(data, num_processes):
    try:
        chunk_size = int(data.shape[0] / num_processes) + 1
        chunks = [data.iloc[i:i + chunk_size, :] for i in range(0, data.shape[0], chunk_size)]
        pool = Pool(processes=num_processes)
        results_parallel = pool.map(slice_haplotype_sequence, chunks)
    finally:
        pool.close()
        pool.join()
    pool.terminate()
    columns=['eval_group',
             'eval_window',
             'chr',
             'start',
             'end',
             'ref_string',
             'tokens',
             'hapId',
             'taxaList']
    frames = [pd.DataFrame(evaluations, columns=columns) for evaluations in results_parallel]
    return pd.concat(frames, ignore_index=True)


def mp_find_peak_prob(data, num_processes):
    evaluation_groups = data['eval_group'].sort_values().unique().tolist()
    try:
        chunk_size = int(len(evaluation_groups) / num_processes) + 1
        evaluation_groups = [evaluation_groups[i: i+chunk_size] for i in range(0, len(evaluation_groups), chunk_size)]
        chunks = [data[data['eval_group'].isin(chunk)] for chunk in evaluation_groups]
        pool = Pool(processes=num_processes)
        results_parallel = pool.map(apply_filter_probs_to_df, chunks)
    finally:
        pool.close()
        pool.join()
    pool.terminate()
    frames = [evaluation for evaluation in results_parallel if not evaluation.empty] #filter out groups that didn't pass the probability threshold
    return pd.concat(frames)    


#functions to wrap the apply which also decrease the looping but can be unfriendly with memory
def apply_filter_probs_to_df(tmp_df):
    columns = ["eval_group",
               "chr",
               "start",
               "end",
               "ref_string",
               "hapId",
               "taxaList",
               "probability"]
    filter_probs_df = pd.DataFrame(tmp_df.groupby('eval_group').apply(filter_probs_to_df_group).reset_index(drop=True)[columns])
    return filter_probs_df

    
#Zack code for pyTASSEL
def createHaplotypeGraph(configFile, methods, includeSequence = False, includeVariants = False) :
    print("{}: start createHaplotypeGraph".format(datetimenow()))
    graphBuilder = javabridge.JWrapper(javabridge.make_instance("net/maizegenetics/pangenome/api/HaplotypeGraphBuilderPlugin","(Ljava/awt/Frame;Z)V",None,False))
    graphBuilder = graphBuilder.configFile(configFile) \
                        .setParameter(javabridge.get_env().new_string(u"methods")
                                      ,javabridge.get_env().new_string(methods)) \
                        .setParameter(javabridge.get_env().new_string(u"includeSequences")
                                      ,includeSequence) \
                        .setParameter(javabridge.get_env().new_string(u"includeVariantContexts"),
                                     includeVariants)
                        
                        
    print("{}: end createHaplotypeGraph".format(datetimenow()))
    return graphBuilder.build()
    
def createRefRangeDF(graph) :
    #Try to mirror rTASSEL's DF for reference ranges
    print("{}: start createRefRangeDF".format(datetimenow()))
    refRangeList = list()
    max_for_bar = graph.referenceRanges().size()
    progress_bar = Bar('createRefRangeDF', max=max_for_bar)
    refRangeIt = graph.referenceRanges().iterator()
    while refRangeIt.hasNext() == True:
        refRange = refRangeIt.next()
        progress_bar.next()
        currentRefRangeOutput = [refRange.id(),
                                 refRange.chromosome().getName(),
                                 refRange.start(),
                                 refRange.end(),
                                 refRange.referenceName(),
                                 len(graph.nodes(refRange))] # add id, chrom name, start, end, refLineName, numNodes
        
        refRangeList.append(currentRefRangeOutput)
    progress_bar.finish()
    return pd.DataFrame(refRangeList, columns=["refRangeId","chr","start","end","refLineName","numNodes"])
    
def createHaplotypeDF(graph, includeSequence = False, includeVariants = False, chrToKeep = []) :
    # Loop through the graph and extract out the haplotypes sequence and variants to a pandas dataframe for additional processing
    hapSeqList = list()
    max_for_bar = graph.referenceRanges().size() #accessing the collection (I hope)
    progress_bar = Bar('createHaplotypeDF', max=max_for_bar)
    refRangeIt = graph.referenceRanges().iterator()
    
    # Do a list comprehension to cast all the objects in chrToKeep to string so the next comparison will work
    chrToKeep = [str(chrom) for chrom in chrToKeep]
    print("{}: start createHaplotypeDF".format(datetimenow()))
    while refRangeIt.hasNext() == True:
        refRange = refRangeIt.next()
        progress_bar.next()
        #PEP 8 recommendation to test if a list is empty 
        if not chrToKeep or refRange.chromosome().getName() in chrToKeep :
            for node in graph.nodes(refRange) :
                taxonOutput = ','.join(map(lambda taxon: taxon.getName(), node.taxaList()))
                currentList = [node.referenceRange().chromosome().getName(),
                               node.referenceRange().start(),
                               node.referenceRange().end(),
                               node.id(),
                               taxonOutput]
                if includeSequence:
                    currentList.append(node.haplotypeSequence().sequence())
                if includeVariants :
                    varInfosOptional = node.variantInfos()
                    if varInfosOptional.isPresent() : 
                        currentList.append(varInfosOptional.get())
                hapSeqList.append(currentList)
    progress_bar.finish()
    columns=["chr","start","end","hapId","taxaList"]
    if includeSequence:
        columns.append("sequence")
    if includeVariants : 
        columns.append("variantInfos")
    return pd.DataFrame(hapSeqList, columns=columns)

    

if __name__ == '__main__':
    chromosome = int(sys.argv[1])  #add argparser to capture chromosome from the command line input
    window_size = int(sys.argv[2].strip())  #add argparser to capture window_size from the command line input
    overlap_window = int(sys.argv[3].strip())  #add argparser to capture overlap_window from the command line input
    output_file =str(sys.argv[4]) #add argparser to capture the name of the output_file (and path) from command line input
    
    kmerlength = 7 #add argparser to capture kmerlength from the command line input
    includeSequence = True #add argparser to capture includeSequence from the command line input
    includeVariants = False #add argparser to capture includeVariants from the command line input
    set_kmerlength(kmerlength) 
    set_overlap(overlap_window)
    set_winsize(window_size) 
    set_includeSequence(includeSequence) 
    set_includeVariants(includeVariants) 
    
    tasselJar = "tassel-5-standalone/sTASSEL.jar"
    tasselDependencies = glob.glob("tassel-5-standalone/lib/*.jar")
    configFilePath = "/local/workdir/mm2842/config.txt" #add argparser to capture configFilePath from the command line input
    methodName = "mummer4,refRegionGroup"  #add argparser to capture methodName from the command line input
        
    javabridge.start_vm(class_path=javabridge.JARS+tasselDependencies+[tasselJar],
                                run_headless=True,
                                max_heap_size="100G") #add argparser to capture max_heap_size from the command line input
    print("{}: VM has started".format(datetimenow()))
    try:
        graph = createHaplotypeGraph(configFile = configFilePath, 
                                     methods = methodName, 
                                     includeSequence = get_includeSequence(), 
                                     includeVariants= get_includeVariants())
        
        print("{}: got a graph with {} nodes".format(datetimenow(), graph.numberOfNodes()))
        #refRangeDF = createRefRangeDF(graph = graph) #only ranges not sequence or variants information
        df = createHaplotypeDF(graph = graph, 
                               includeSequence = get_includeSequence(),
                               includeVariants = get_includeVariants(), 
                               chrToKeep=[chromosome] )
        print("{}: got a dataframe with shape {} rows, {} columns".format(datetimenow(), df.shape[0], df.shape[1]))
    except Exception as e:
        print(e)
        javabridge.kill_vm()

    
    # Convert list of list to pandas data frame then it should work with Katherine's model
    df["start"] = pd.to_numeric(df["start"])
    df['idx_row'] = df.index
    hapIDTotal = df['hapId'].sort_values().unique().shape[0]
    print(df.head(1))
    print("{}: k-size set to {}".format(datetimenow(), get_kmerlength()))
    print("{}: window size set to {}".format(datetimenow(), get_winsize()))
    print("{}: window overlap size set to {}".format(datetimenow(), get_overlap()))
    
    # load pretrained bok model from disk  #add argparser to capture model from the command line input- be sure that we can use all the models that kmergrammar can generate
    bok_model_dir = '/local/workdir/mm2842/' 
    bok_model_file = bok_model_dir + 'Kgrammar_lexicon_model_LR_mode_filtered_coreprom_{}.pkl'.format(kmerlength)
    TFIDF_LR = joblib.load(bok_model_file) 
    
    file_out = '/local/workdir/mm2842/Hackathon/' + output_file
    
    print("{}: Input haplotypes {}".format(datetimenow(), hapIDTotal))
    print("{}: Tokenizing sequences in parallel".format(datetimenow()))
    chunk_size = 5000 
    chunks = [df.iloc[i:i + chunk_size, :] for i in range(0, df.shape[0], chunk_size)]
    max_for_bar = len(chunks)
    progress_bar = Bar('Slice haplotypes', max=max_for_bar)

    frames = list()
    num_processes = cpu_count() - 2 #add argparser to capture num_processes from the command line input

    for chunk in chunks:
        frames.append(mp_slice_haplotype(chunk, num_processes))
        progress_bar.next()
    progress_bar.finish()	
    
    tmp_eval_df = pd.concat(frames, ignore_index=True)
    print("{}: Remaining haplotypes {}".format(datetimenow(), tmp_eval_df['hapId'].sort_values().unique().shape[0]))
    eval_tokens = tmp_eval_df["tokens"].values

    print("{}: Wrote kmers for a total of {} sequences".format(datetimenow(), len(eval_tokens)))
    kmer_names = building_vocabulary(full_mode=False)
    feature_names = np.asarray(kmer_names)
    vectorizer = TfidfVectorizer(min_df=1, 
								 max_df=1.0, 
								 sublinear_tf=True, 
								 use_idf=True,
								 vocabulary=kmer_names)
    
    print("{}: Transform raw counts to TF*IDFs".format(datetimenow()))     
    X_TFIDF = vectorizer.fit_transform(eval_tokens)
    
    print("{}: Predictions with bag-of-k-mers".format(datetimenow()))  
    tmp_eval_df['prediction'] = TFIDF_LR.predict(X_TFIDF)
    tmp_eval_df['probability'] = TFIDF_LR.predict_proba(X_TFIDF)[:, 1] 

    print("{}: Filtering to get the good predictions".format(datetimenow()))
    evaluation_groups = tmp_eval_df['eval_group'].sort_values().unique().tolist()

    chunk_size = 5000
    eval_chunks = [evaluation_groups[i: i+chunk_size] for i in range(0, len(evaluation_groups), chunk_size)]
    promoter_predictions = list()
    max_for_bar = len(eval_chunks) #accessing the collection (I hope)
    progress_bar = Bar('Filtering predictions', max=max_for_bar)
    for chunk in eval_chunks:
        tmp_df = tmp_eval_df[tmp_eval_df['eval_group'].isin(chunk)]
        promoter_predictions.append(mp_find_peak_prob(tmp_df, num_processes))
        progress_bar.next()
    progress_bar.finish()	
    
    predictions_df = pd.concat(promoter_predictions, ignore_index=True)     
    predictions_df.to_csv(file_out,sep="\t",index=False)
    predicted = len(predictions_df['hapId'].sort_values().unique().tolist())
    non_predicted = tmp_eval_df['hapId'].sort_values().unique().shape[0] - predicted
    
    print("{}: input haplotypes: {}".format(datetimenow(), hapIDTotal))
    print("{}: haplotypes with predicted core promoter: {}".format(datetimenow(), predicted))
    print("{}: haplotypes without predicted core promoter {}".format(datetimenow(), non_predicted))
    print()
    javabridge.kill_vm()
